var browserify = require('browserify')
var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var concat = require('gulp-concat');
var rename = require('gulp-rename'); 

var sassPaths = [
  'node_modules/bootstrap-sass/assets/stylesheets'
];
var assetPath = 'assets/';

gulp.task('sass', function() {
    return gulp.src(assetPath+'/sass/app.scss')
        .pipe($.sass({
		  includePaths: sassPaths,
		  outputStyle: 'compressed' // if css compressed **file size**
        }))
        .pipe(gulp.dest(assetPath+'/dist/css'))
});
 
gulp.task('scripts', function() {
  return gulp.src([
  			assetPath+'/js/jquery-3.2.1.min.js', 
  			assetPath+'/js/bootstrap.min.js',
  			assetPath+'/js/main.js', 
  			// assetPath+'/js/owl.carousel.min.js',
  			// assetPath+'/js/jquery.countTo.js',
  			//assetPath+'/js/jquery.stellar.min.js',
  			// assetPath+'/js/simplyCountdown.js',
  			// assetPath+'/js/main.js'
  		])
    .pipe(concat('app.js'))
    .pipe(gulp.dest(assetPath+'/dist/js'))
	.pipe(rename('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(assetPath+'/dist/js'));
});

/* watches */
gulp.task('default', ['sass', 'scripts']);
gulp.watch([assetPath+'/sass/**/*.scss'], ['sass']);
gulp.watch([assetPath+'/js/**/*.js'], ['scripts']);